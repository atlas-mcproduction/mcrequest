# Test MC Requests

A template repository for testing MC requests using the [MCProdTester](https://gitlab.cern.ch/atlas-mcproduction/MCProdTester/) project.

It can be used in two ways:
- Making a fork and setting up the grid certificate for private tests.
- Requesting Developer access to this repository and using a separate branch.

Instructions for creating a request YAML definition are in the MCProdTester README.

## Forking Repository
If you fork the repository, you need to setup the following CI variables using your grid certificate:
- `GRID_CERT`: Base64 encoded value of your grid certificate (`base64 -i ~/.globus/usercern.pem`).
- `GRID_KEY`: Base64 encoded value of your grid key (`base64 -i ~/.globus/userkey.pem`).
- `GRID_PASS`: Your certificate password surrounded by `=`. (ie: `=password=`).

## Custom Branch (Recommended)
The recommended way to prepare a request test is to commit directly to a branch of this repository. This has the following avantages:
- No need to setup the grid certificate. All is ready to go!
- Central place to monitor all requests.

The use this repostory:
1. Email to request `Maintainer` role from the repository owner (most likely kkrizka@cern.ch).
2. Clone the repository.
```shell
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-mcproduction/mcrequest.git
```
3. Enter the repository and create your custom branch. Pushes to `main` are protected.
```shell
cd mcrequest
git checkout -b ${USER}_requestname
```
4. Define the request in the provided `request.yaml` file and run local tests. Instructions are available in the [MCProdTester](https://gitlab.cern.ch/atlas-mcproduction/MCProdTester/) README.
5. Commit your changes and push the branch to GitLab.
6. Create a MR to `main` with your changes. The MR can be used to discuss with experts any errors in the failing CI pipeline.
7. Continue updating the request with changes as necessary. For example, update to offical tags once any custom changes are registered to AMI.
8. One the request is submitted, close the MR.
